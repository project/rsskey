
Description
-----------

The RSSKey module provides rss feeds for selected content (e.g. story or
forum) and taxonomy, authenticated by a cryptographic key that
is included in the feed url. This permits rss aggregators to 
get useful feeds from a drupal installation that does not allow
anonymous users to view content, or that varies content permitted
by user.

The keys are unique to each user and site, and are based on an md5
hash of the user's password, and a site-specific secret.
Changing the site-specific secret changes all keys, invalidating
any key-based urls previously in use by any user. A user changing
their password changes all of that user's keys only.

Feed urls are published to the Syndication module's page for listing.

Feed URLs take the form:

rsskey/UID/KEY/story                  (front page stories)
rsskey/UID/KEY/forum/TERM/DEPTH       (forum content)

The site secret is configured in Administer..Settings..Rsskey

Security implications
---------------------
If a key string is compromised, an outsider may be able to view insider
content with another user's permissions, but that's probably not a huge
disaster, as it will be limited by what kind of content will be provided,
and will be read-only. 

It should not be feasible to determine the site secret based on a known
plaintext attack with a key string, user name, and password.  Similarly,
knowing the key string, site secret, and a user name should not make it
feasible to determine a user's (encrypted) password without brute force.

The module logs all user accesses.
  

Installation
------------

1. Copy rsskey.module into the modules directory.

2. Enable this module by navigating to:

     administer > modules

3. After enabling this module in the previous step, navigate to

     administer > settings > rsskey

   to set the site secret.


Credit
------

Written by: Alan Schwartz <alansz@uic.edu>
Released under the GNU General Public License (see LICENSE.TXT)

Thanks to James Black for the adminrss module on which this was based.

History
-------

2006-06-22
 - initial development

